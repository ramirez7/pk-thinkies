# The Easiest Way to Get to Ledge

One of the most universal edgeguarding techniques is to grab ledge and cover options with ledge getup options + ledgehop aerials. Being on the ledge gives you 29 frames of intangibility, and by releasing ledge you can hit locations above, away from, and below the stage with aerials. And of course, being on the ledge means your opponent cannot grab it.

You often have to get to ledge off a hit confirm. Maybe you hit a space animal with a Sheik needle. Or you use a throw to get your opponent off-stage.

In all of these situations, you have to react and move quickly to get to ledge in time to be in position to edgeguard. There are a variety of ways to get to ledge, all with varying degrees of difficulty. WD back, moonwalk, PC drop, SH needle reverse, etc.

However, the absolute *easiest* way to get to ledge is to:

1. Roll towards ledge so that your roll takes you to the very tip of the stage.
   - This will put your back to the ledge.
2. Short hop.
3. (Optional) Drift slightly outward.
   - This is character- and roll- dependent. Some rolls put your ECB in a location where you won't need to drift.
   - Sheik's forward roll doesn't need drift, but backwards roll does.
   - Fox's backwards roll does need drift, but forward roll doesn't.
   - Neither of Ness' rolls need drift.
4. (Optional) Fast fall a bit to speed things up.
   - Make sure to let go of down quick enough to not go past the ledge.
   - The higher the SH or floatier the character, the more important this is.
5. Fall to ledge.

The only downside of this method is that it is somewhat slow. For example, for Sheik:

- Roll is 31 frames
- SH takes about 15 frames to reach its peak
- Then it takes about about 15 more (fewer if FF'd) to get to ledge

I didn't measure precisely, but that's about one second total time to get to ledge if you are already in roll range.

In most situations, one second is plenty fast to get to ledge. Oftentimes you'll have more than that and will be trying to time when you grab the ledge anyways (to time your i-frames). So if you can do something this easy, why not? It requires easier inputs (less likely to mess up), fewer inputs (slight break for your hands), and less focus (your brain and eyes can focus on reading your opponent's recovery).

This method is also very accessible from a variety of states - you can roll out of any actionable grounded state and the method doesn't care about which way you're facing.

Credit Voorhese from Indiana for teaching me this technique.
