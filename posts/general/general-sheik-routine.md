# General Sheik Practice Routine

- **Movement Training**
  - Wavedash Trainer
  - Platform Drills
  - Transitioning to/from crouch
  - Acting out of pokes
  - Free Movement
  - Eggs-ercise
- **Defensive Drills**
  - Reversal Training
  - Slide-Off Training
  - Shield Drop Training
- **Punish Training**
  - Out-of-Crouch Practice
  - Armada Shine Training
  - Combo Trainer

## Movement Training
### Wavedash Trainer

Go `Wavedash Training` and practice WDing across stages and pay attention to how Uncle Punch is rating your WDs.

### KirbyKaze Movement Drills

- [Part 1](https://kirbykazemeleeblog.blogspot.com/2014/10/diy-ssbm-movement-drills-part-12.html)
- [Part 2](https://kirbykazemeleeblog.blogspot.com/2014/11/diy-ssbm-movement-drills-part-22.html)

### Platform Drills

Practice getting onto and off of platforms.

**Getting On:**
- Jump Waveland
- On Yoshi's or Pokemon: SH empty land

**Getting Off:**
- Run off (into aerial or into FF WL)
- Dash shield drop
- SH and FF through
  - This is especially useful if you charge needles in between to trick your opponent
- Wavedash backwards off
  - If your character "trips," it's because you held L too long and were shielding during the fall off
**Mixing it up:**
Chain them together!
- Dash shield drop and then immediately DJ WL
- WL onto platform, buffer shield (while holding side) into shield drop

### Transitioning to/from crouch

- Go in `Training Lab`
- Crouch and practice dashing in both direction out of it
  - You have to quickly move your stick from the bottom notch to the left/right notch without going through center.
  - It's easiest quickly to roll along the gate. This helps it be smooth.
- Practice entering crouch out of run
- Chain them together.
  - Crouch, dash, crouch-out-of-run, dash, etc.
  - The hardest is to dash backwards out of crouch, crouch, dash backwards again, etc.

### Needle reverse

The inputs to needle reverse:
- Be airborne.
- Hit the stick in the reverse direction.
- Let the stick return to neutral.
- Hit and hold B.
- Hit Z or L or R.

You can slow things down & practice these inputs in partial chunks:
- Practice started & stopping needles quickly by hitting B and then L/Z/R.
  - You can do this grounded or in the air.
- Practice reverse throwing needles.
  - Jump, flick the stick behind you hit B. Sheik should turn around and throw needles.

### Acting out of pokes
Do a poke (ftilt, SH fair, SH bair) and then practice doing something else quickly after it:
- Dash back
- Dash forward
- Dash back and then dash forward to where you started
- WD back
- Ftilt after aerials
- Buffer shield
- Buffer crouch
- Jab
- Boost grab

### Eggs-ercise

Do this with fair. When the eggs are low, you'll have to SHFFLC fair. But the eggs bounce and fall from varying heights, so if you're quick you can catch them at the peak of your SH or even higher with FH+DJ.

See how high of a score you can get!

## Defensive Drills

### Reversal (WD OoS) Training

- Go to `Lv. 7` `Reveral Training`
- Pick a character you want to practice against
- Pause and set their attack to what you want to practice punishing:
  - Marth: forward smash
  - Peach: down smash
  - Fox: up smash
  - Falcon: Side-b
- Shield them ove and WD OoS into grab or dsmash to punish
  - Grab is juicier but dsmash is faster
  - You may need to dash JC grab after WD depending on distance

### Slide-Off Training

- Go to `Lv. 14` `Slide-Off Training`
- In this stage, you will enter knockdown on a platform (as if uthrown by Marth) and then get hit by Marth uair.
- The goal is to punish with "slide off." To do it you:
  - Hold the cstick down
  - Hold the control stick towards the nearest platform edge.
- If you do it right, you'll slide off the platform and be actionable.
- Start by trying it on missed tech (DI towards ledge beforehand).
- Then, try tech rolling to one side or the other and sliding off in that direction.

### Shield Drop Training

- Go to `Lv. 12` `Shield Drop Training`
- Pick a character to practice against
- Shield drop punish the attack
- It helps to microdash shield to set up the shield drop input.
- You can pause to change which direction you're facing (to practice fair vs bair shield drops)
- You can use dpad left/right to move closer/farther apart.

## Punish Training
### Crouch Tech Chasing

- Go to `Training Lab`
- Select Fox, Falco, or Falcon to be against
  - This is because they get knocked down by dthrow at 0%
- Go to any stage
- Pause and go to `CPU Options`:
  - Set `Tech Option` to `None`
  - Set `Get Up Option` to `Attack` to start
- Push the enemy to the center of the stage
- Hit `right dpad` to save the state.
- Dthrow
- Crouch next to the missed tech
- Wait for the enemy to getup attack you
- Out of crouch cancel, dash into boost/JC grab them

You can repeat this drill but for other `Get Up Option`s:
- For `Towards` or `Away`, dash out of crouch and boost/JC grab.
- For `Stand`, JC grab

Then put it all together with `Random`.

### Combo Trainer

Pick various characters and set their percents (L + up/down = 10% increments. L + left/right = 1% increments).

See what works and what doesn't!

Some good ones to practice:

- Fox/Falco/CF tech chasing from 0%
- Fox/Falco/CF ftilt/dtilt tech chasing
  - 30-60% is a good place to try it
  - Sometimes they can DI away and you can't cover everything
  - Pay attention and learn the nuances
- Marth dthrow punishes (all percents)
  - DI away: CG
  - Slight DI in: util (low %s) uair (high %s)
  - DI away at high %: Quick SH slap (I like to use the A button) into edgeguard.
  - Suboptimal but good if they DI in as a mixup: ftilt
  - On certain stages, you'll have to platform tech chase:
    - WL/empty land on platform and boost grab
- Peach/Puff dthrow fair/uair
- CG Yoshi, Ganon, etc
  - It starts at 0% for Yoshi. Needs a few % for Ganon. Try stuff out!
  - Dash (non-boost) grab works great. It grabs behind you and is lenient.
  - Eventually he'll land on platforms. WL regrab those.

Mix up stages for different platform heights (or no plats with FD - great for brutal CGs)

### Armada Shine Training

A simple edgeguard flowchart for firefox at/below ledge level is:

1. Hit them with a needle.
2. Immediately get to ledge with WD.
3. Let go of ledge, fall (maybe FF), and DJ bair to hit them out of firefox (either the charge or after it fires depending on where they do it)
4. Get back to ledge and rinse and repeat.
